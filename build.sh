#!/usr/bin/env bash
set -euo pipefail
cd "$(dirname "$0")"

function dockerRun {
  docker run \
    --rm \
    -i \
    -u $(id -u):$(id -g) \
    -v "$PWD:/app" \
    -w "/app" \
    --entrypoint bash \
    node:19
}

cat << "HEREDOC" | dockerRun
set -euxo pipefail
mkdir -p output/
cp $(command -v node) output/hello
cp $(command -v node) output/hello-mac
npx postject output/hello NODE_JS_CODE hello.js \
    --sentinel-fuse NODE_JS_FUSE_fce680ab2cc467b6e072b8b5df1996b2
npx postject output/hello-mac NODE_JS_CODE hello.js \
    --sentinel-fuse NODE_JS_FUSE_fce680ab2cc467b6e072b8b5df1996b2 \
    --macho-segment-name NODE_JS
set +x
echo "Done, binaries are:"
ls -lh output/
HEREDOC
