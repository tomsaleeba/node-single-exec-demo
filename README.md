This is a demo of the "build a single executable from your nodejs app" that is outlined at
[https://nodejs.org/api/single-executable-applications.html]().

# How to use
- clone this repo
- make sure you have `docker` installed
- `bash build.sh`
- the binaries will be in `output/`. Use the `-mac` suffixed one for MacOS, and the other
  binary for linux (and maybe Windows?)

# TODO
- add separate file dependency in this project
- add third-party dependency (from npm)


# Reference
Similar to [https://deno.land/manual@v1.31.1/tools/compiler]().
